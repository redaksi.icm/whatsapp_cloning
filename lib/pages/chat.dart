import 'package:flutter/material.dart';
import 'data/chat_model.dart';

class LayarChat extends StatefulWidget {
  @override
  _LayarChatState createState() => _LayarChatState();
}

class _LayarChatState extends State<LayarChat> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: contohData.length,
      itemBuilder: (context, i) => Column(
            children: <Widget>[
              Divider(
                height: 10.0,
              ),
              ListTile(
                leading: CircleAvatar(
                  foregroundColor: Theme.of(context).primaryColor,
                  backgroundColor: Colors.grey,
                  backgroundImage: NetworkImage(contohData[i].urlAvatar),
                  //backgroundImage: ImageProvider.(contohData[i].urlAvatar),
                ),
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      contohData[i].nama,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(
                      contohData[i].waktu,
                      style: TextStyle(color: Colors.grey, fontSize: 14.0),
                    ),
                  ],
                ),
                subtitle: Container(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Text(
                    contohData[i].pesan,
                    style: TextStyle(color: Colors.grey, fontSize: 15.0),
                  ),
                ),
              )
            ],
          ),
    );
  }
}
